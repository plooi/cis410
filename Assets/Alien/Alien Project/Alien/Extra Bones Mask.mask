%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &101100000
AvatarMask:
  m_ObjectHideFlags: 0
  m_PrefabParentObject: {fileID: 0}
  m_PrefabInternal: {fileID: 0}
  m_Name: Extra Bones Mask
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Alien LP
    m_Weight: 1
  - m_Path: Alien LP/Eyes
    m_Weight: 1
  - m_Path: Alien LP/Hip Armor
    m_Weight: 1
  - m_Path: Alien LP/Neck Armor
    m_Weight: 1
  - m_Path: Alien LP/Sword Holder
    m_Weight: 1
  - m_Path: Alien_
    m_Weight: 1
  - m_Path: Alien_/Spear Controller
    m_Weight: 1
  - m_Path: Alien_/Spear Controller/Spear
    m_Weight: 1
  - m_Path: Alien_ChestGizmo
    m_Weight: 1
  - m_Path: Alien_HeadGizmo
    m_Weight: 1
  - m_Path: Alien_Hips
    m_Weight: 1
  - m_Path: Alien_Hips/Alien_L_Upper_Leg
    m_Weight: 1
  - m_Path: Alien_Hips/Alien_L_Upper_Leg/Alien_L_Lower_Leg
    m_Weight: 1
  - m_Path: Alien_Hips/Alien_L_Upper_Leg/Alien_L_Lower_Leg/Alien_L_Ankle
    m_Weight: 1
  - m_Path: Alien_Hips/Alien_L_Upper_Leg/Alien_L_Lower_Leg/Alien_L_Ankle/Alien_Toes
    m_Weight: 1
  - m_Path: Alien_Hips/Alien_R_Upper_Leg
    m_Weight: 1
  - m_Path: Alien_Hips/Alien_R_Upper_Leg/Alien_R_Lower_Leg
    m_Weight: 1
  - m_Path: Alien_Hips/Alien_R_Upper_Leg/Alien_R_Lower_Leg/Alien_R_Ankle
    m_Weight: 1
  - m_Path: Alien_Hips/Alien_R_Upper_Leg/Alien_R_Lower_Leg/Alien_R_Ankle/Alien_Toes
      1
    m_Weight: 1
  - m_Path: Alien_Hips/Alien_Spine
    m_Weight: 1
  - m_Path: Alien_Hips/Alien_Spine/Alien_Chest
    m_Weight: 1
  - m_Path: Alien_Hips/Alien_Spine/Alien_Chest/Alien_L_Shoulder
    m_Weight: 1
  - m_Path: Alien_Hips/Alien_Spine/Alien_Chest/Alien_L_Shoulder/Alien_L_Upper_Arm
    m_Weight: 1
  - m_Path: Alien_Hips/Alien_Spine/Alien_Chest/Alien_L_Shoulder/Alien_L_Upper_Arm/Alien_L_Lower_Arm
    m_Weight: 1
  - m_Path: Alien_Hips/Alien_Spine/Alien_Chest/Alien_L_Shoulder/Alien_L_Upper_Arm/Alien_L_Lower_Arm/Alien_L_Hand
    m_Weight: 1
  - m_Path: Alien_Hips/Alien_Spine/Alien_Chest/Alien_L_Shoulder/Alien_L_Upper_Arm/Alien_L_Lower_Arm/Alien_L_Hand/Alien_L_Forefinger1
    m_Weight: 1
  - m_Path: Alien_Hips/Alien_Spine/Alien_Chest/Alien_L_Shoulder/Alien_L_Upper_Arm/Alien_L_Lower_Arm/Alien_L_Hand/Alien_L_Forefinger1/Alien_L_Forefinger2
    m_Weight: 1
  - m_Path: Alien_Hips/Alien_Spine/Alien_Chest/Alien_L_Shoulder/Alien_L_Upper_Arm/Alien_L_Lower_Arm/Alien_L_Hand/Alien_L_Forefinger1/Alien_L_Forefinger2/Alien_L_Forefinger3
    m_Weight: 1
  - m_Path: Alien_Hips/Alien_Spine/Alien_Chest/Alien_L_Shoulder/Alien_L_Upper_Arm/Alien_L_Lower_Arm/Alien_L_Hand/Alien_L_Middle1
    m_Weight: 1
  - m_Path: Alien_Hips/Alien_Spine/Alien_Chest/Alien_L_Shoulder/Alien_L_Upper_Arm/Alien_L_Lower_Arm/Alien_L_Hand/Alien_L_Middle1/Alien_L_Middle2
    m_Weight: 1
  - m_Path: Alien_Hips/Alien_Spine/Alien_Chest/Alien_L_Shoulder/Alien_L_Upper_Arm/Alien_L_Lower_Arm/Alien_L_Hand/Alien_L_Middle1/Alien_L_Middle2/Alien_L_Middle3
    m_Weight: 1
  - m_Path: Alien_Hips/Alien_Spine/Alien_Chest/Alien_L_Shoulder/Alien_L_Upper_Arm/Alien_L_Lower_Arm/Alien_L_Hand/Alien_L_Thumb1
    m_Weight: 1
  - m_Path: Alien_Hips/Alien_Spine/Alien_Chest/Alien_L_Shoulder/Alien_L_Upper_Arm/Alien_L_Lower_Arm/Alien_L_Hand/Alien_L_Thumb1/Alien_L_Thumb2
    m_Weight: 1
  - m_Path: Alien_Hips/Alien_Spine/Alien_Chest/Alien_L_Shoulder/Alien_L_Upper_Arm/Alien_L_Lower_Arm/Alien_L_Hand/Alien_L_Thumb1/Alien_L_Thumb2/Alien_L_Thumb3
    m_Weight: 1
  - m_Path: Alien_Hips/Alien_Spine/Alien_Chest/Alien_Neck
    m_Weight: 1
  - m_Path: Alien_Hips/Alien_Spine/Alien_Chest/Alien_Neck/Alien_Head
    m_Weight: 1
  - m_Path: Alien_Hips/Alien_Spine/Alien_Chest/Alien_R_Shoulder
    m_Weight: 1
  - m_Path: Alien_Hips/Alien_Spine/Alien_Chest/Alien_R_Shoulder/Alien_R_Upper_Arm
    m_Weight: 1
  - m_Path: Alien_Hips/Alien_Spine/Alien_Chest/Alien_R_Shoulder/Alien_R_Upper_Arm/Alien_R_Lower_Arm
    m_Weight: 1
  - m_Path: Alien_Hips/Alien_Spine/Alien_Chest/Alien_R_Shoulder/Alien_R_Upper_Arm/Alien_R_Lower_Arm/Alien_R_Hand
    m_Weight: 1
  - m_Path: Alien_Hips/Alien_Spine/Alien_Chest/Alien_R_Shoulder/Alien_R_Upper_Arm/Alien_R_Lower_Arm/Alien_R_Hand/Alien_R_Forefinger1
    m_Weight: 1
  - m_Path: Alien_Hips/Alien_Spine/Alien_Chest/Alien_R_Shoulder/Alien_R_Upper_Arm/Alien_R_Lower_Arm/Alien_R_Hand/Alien_R_Forefinger1/Alien_R_Forefinger2
    m_Weight: 1
  - m_Path: Alien_Hips/Alien_Spine/Alien_Chest/Alien_R_Shoulder/Alien_R_Upper_Arm/Alien_R_Lower_Arm/Alien_R_Hand/Alien_R_Forefinger1/Alien_R_Forefinger2/Alien_R_Forefinger3
    m_Weight: 1
  - m_Path: Alien_Hips/Alien_Spine/Alien_Chest/Alien_R_Shoulder/Alien_R_Upper_Arm/Alien_R_Lower_Arm/Alien_R_Hand/Alien_R_Middle1
    m_Weight: 1
  - m_Path: Alien_Hips/Alien_Spine/Alien_Chest/Alien_R_Shoulder/Alien_R_Upper_Arm/Alien_R_Lower_Arm/Alien_R_Hand/Alien_R_Middle1/Alien_R_Middle2
    m_Weight: 1
  - m_Path: Alien_Hips/Alien_Spine/Alien_Chest/Alien_R_Shoulder/Alien_R_Upper_Arm/Alien_R_Lower_Arm/Alien_R_Hand/Alien_R_Middle1/Alien_R_Middle2/Alien_R_Middle3
    m_Weight: 1
  - m_Path: Alien_Hips/Alien_Spine/Alien_Chest/Alien_R_Shoulder/Alien_R_Upper_Arm/Alien_R_Lower_Arm/Alien_R_Hand/Alien_R_Thumb1
    m_Weight: 1
  - m_Path: Alien_Hips/Alien_Spine/Alien_Chest/Alien_R_Shoulder/Alien_R_Upper_Arm/Alien_R_Lower_Arm/Alien_R_Hand/Alien_R_Thumb1/Alien_R_Thumb2
    m_Weight: 1
  - m_Path: Alien_Hips/Alien_Spine/Alien_Chest/Alien_R_Shoulder/Alien_R_Upper_Arm/Alien_R_Lower_Arm/Alien_R_Hand/Alien_R_Thumb1/Alien_R_Thumb2/Alien_R_Thumb3
    m_Weight: 1
  - m_Path: Alien_Hips/Alien_Tail1
    m_Weight: 1
  - m_Path: Alien_Hips/Alien_Tail1/Alien_Tail2
    m_Weight: 1
  - m_Path: Alien_Hips/Alien_Tail1/Alien_Tail2/Alien_Frontskirt3
    m_Weight: 1
  - m_Path: Alien_Hips/Alien_Tail1 1
    m_Weight: 1
  - m_Path: Alien_Hips/Alien_Tail1 1/Alien_Backskirt2
    m_Weight: 1
  - m_Path: Alien_HipsGizmo
    m_Weight: 1
  - m_Path: Alien_L_Forefinger1Gizmo
    m_Weight: 1
  - m_Path: Alien_L_Forefinger2Gizmo
    m_Weight: 1
  - m_Path: Alien_L_Forefinger3Gizmo
    m_Weight: 1
  - m_Path: Alien_L_HandGizmo
    m_Weight: 1
  - m_Path: Alien_L_Lower_ArmGizmo
    m_Weight: 1
  - m_Path: Alien_L_Middle1Gizmo
    m_Weight: 1
  - m_Path: Alien_L_Middle2Gizmo
    m_Weight: 1
  - m_Path: Alien_L_Middle3Gizmo
    m_Weight: 1
  - m_Path: Alien_L_Platform
    m_Weight: 1
  - m_Path: Alien_L_Thumb1Gizmo
    m_Weight: 1
  - m_Path: Alien_L_Thumb2Gizmo
    m_Weight: 1
  - m_Path: Alien_L_Thumb3Gizmo
    m_Weight: 1
  - m_Path: Alien_L_Upper_ArmGizmo
    m_Weight: 1
  - m_Path: Alien_R_Forefinger1Gizmo
    m_Weight: 1
  - m_Path: Alien_R_Forefinger2Gizmo
    m_Weight: 1
  - m_Path: Alien_R_Forefinger3Gizmo
    m_Weight: 1
  - m_Path: Alien_R_HandGizmo
    m_Weight: 1
  - m_Path: Alien_R_Lower_ArmGizmo
    m_Weight: 1
  - m_Path: Alien_R_Middle1Gizmo
    m_Weight: 1
  - m_Path: Alien_R_Middle2Gizmo
    m_Weight: 1
  - m_Path: Alien_R_Middle3Gizmo
    m_Weight: 1
  - m_Path: Alien_R_Platform
    m_Weight: 1
  - m_Path: Alien_R_Thumb1Gizmo
    m_Weight: 1
  - m_Path: Alien_R_Thumb2Gizmo
    m_Weight: 1
  - m_Path: Alien_R_Thumb3Gizmo
    m_Weight: 1
  - m_Path: Alien_R_Upper_ArmGizmo
    m_Weight: 1
  - m_Path: Alien_Tail1Gizmo
    m_Weight: 1
  - m_Path: Alien_Tail1Gizmo 1
    m_Weight: 1
  - m_Path: Alien_Tail2Gizmo
    m_Weight: 1
  - m_Path: Alien_Tail2Gizmo 1
    m_Weight: 1
  - m_Path: Alien_Tail3Gizmo
    m_Weight: 1
  - m_Path: Alien_ToesGizmo
    m_Weight: 1
  - m_Path: Alien_ToesGizmo 1
    m_Weight: 1
