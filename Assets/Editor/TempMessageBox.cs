﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class TempMessageBox
{
    public static void Show(string msg)
    {
         EditorUtility.DisplayDialog("", msg, "Ok");
         
    }
}
