﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Allows this to show up in inspector to modify it
[System.Serializable]

public class Dialogue 
{
    //This is the name of the NPC we are talking to.
    public string name;

    // increases the  Elements boxes to allow for multiple lines.
    [TextArea (3,100) ]

    //stores the sentences we want to load into the Queue
    public string[] sentences;

}
