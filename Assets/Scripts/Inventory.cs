﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    public List<Item> itemList = new List<Item>();
    public ItemDatabase itemData;
    public UIInventory inventoryUI;
    public int myID;
    public Dialogue start;
    private void Start()
    {
        
        
        //AddItem(0);
        AddItem(2);
        for(int i = 0; i <6; i++)AddItem("Diamond Ore");
        inventoryUI.gameObject.SetActive(!inventoryUI.gameObject.activeSelf);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.I))
        {
            inventoryUI.gameObject.SetActive(!inventoryUI.gameObject.activeSelf);
            if (Cursor.lockState == CursorLockMode.Locked)
            {
                Cursor.lockState = CursorLockMode.None;
            }
            else
            {
                Cursor.lockState = CursorLockMode.Locked;
            }
        }

        //Control for Continue 
        if (Input.GetKeyDown(KeyCode.C))
        {
           FindObjectOfType<DialogueManager>().DisplayNextSentence();
        }
       
        // Control for Mute On/OFF
        if (Input.GetKeyDown(KeyCode.M))
        {
           if (AudioListener.volume == 0.0F)
            {
                AudioListener.volume = 0.3F;
            }
            else
            {
                AudioListener.volume = 0.0F;
            }
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            FindObjectOfType<DialogueManager>().RunDialogue6();
        }

        

    }

    public void AddItem(int id)
    {
        Item possiblyAnItem = CheckForItem(id);
        if(possiblyAnItem != null)
        {
            possiblyAnItem.quantity += 1;
            return;
        }
        Item toAdd = itemData.GetItem(id);
        itemList.Add(toAdd);
        inventoryUI.AddItem(toAdd);
        Debug.Log("Added item: " + toAdd.title);
    }
    public void AddItem(string itemName)
    {
        Item possiblyAnItem = CheckForItem(itemName);
        if(possiblyAnItem != null)
        {
            possiblyAnItem.quantity += 1;
            return;
        }
        Item toAdd = itemData.GetItem(itemName);
        itemList.Add(toAdd);
        inventoryUI.AddItem(toAdd);
        Debug.Log("Added item: " + toAdd.title);     
    }
    public Item CheckForItem(int id)
    {
        return itemList.Find(item => item.id == id);
    }
    public Item CheckForItem(string itemName)
    {
        return itemList.Find(item => item.title == itemName);
    }
    public void RemoveItem(int id)
    {
        Item item = CheckForItem(id);
        if(item != null)
        {
            itemList.Remove(item);
            inventoryUI.RemoveItem(item);
            Debug.Log("Item removed: " + item.title);
        }

    }
    public void RemoveItem(Item item)
    {
        
        if(item != null && itemList.Contains(item))
        {
            itemList.Remove(item);
            inventoryUI.RemoveItem(item);
            Debug.Log("Item removed: " + item.title);
        }

    }
}
