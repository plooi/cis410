﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform target;
    public Transform pivot;
    //public Vector3 offset;
    public float offset;
    public bool useOffsetValues;

    public float rotateSpeed;
    
    
    public float desiredYAngle = 0;
    public float desiredXAngle = 90;
    
    public Terrain terrain;
    GameObject alienAndy;
    
    

    // Start is called before the first frame update
    void Start()
    {
        alienAndy = GetTag("AlienAndy")[0];
        
        /*
        if (GlobalControl.Instance.scene == GlobalControl.Environment.Overworld)
        {
            transform.position = new Vector3(433.7f, 15.29f, 292.81f);
        }
        if (!useOffsetValues)
        {
            offset = Vector3.Distance(target.position, transform.position);//target.position - transform.position;
        }*/
        offset = Vector3.Distance(target.position, transform.position);

        pivot.transform.position = target.transform.position;
        pivot.transform.parent = target.transform;

        Cursor.lockState = CursorLockMode.Locked;
        
    }

    // Update is called once per frame
    void LateUpdate()
    {
        desiredYAngle += Input.GetAxis("Mouse X")*rotateSpeed*.42f;//target.eulerAngles.y;
        desiredXAngle += Input.GetAxis("Mouse Y")*rotateSpeed*.42f;//pivot.eulerAngles.x;
        //target.eulerAngles = new Vector3(target.rotation.x, desiredYAngle+180, target.rotation.z);
        
        if(desiredXAngle > 84 && desiredXAngle < 180)
        {
            desiredXAngle = 84;
        }
        if(desiredXAngle < 15)
        {
            desiredXAngle = 15;
        }
        
        
        
        
        //print(desiredYAngle + " " + desiredXAngle);
        
        while(true)
        {
        
            float desiredYAngleConv = unityToUnitCircle(desiredYAngle);
            float desiredXAngleConv = unityToUnitCircle(desiredXAngle);
            
            //Quaternion rotation = Quaternion.Euler(desiredXAngle, desiredYAngle, 0);
            Vector3 rotation = new Vector3(Mathf.Cos(desiredYAngleConv)*Mathf.Cos(desiredXAngleConv), Mathf.Sin(desiredXAngleConv), Mathf.Sin(desiredYAngleConv)*Mathf.Cos(desiredXAngleConv));
            pivot.eulerAngles = new Vector3(desiredXAngle,desiredYAngle+180,0);
            
            
            //print(desiredXAngle);
           
            transform.position = target.position + rotation * offset;
            if(transform.position.y < terrainHeightAtHere()+2)
            {
                //transform.position = new Vector3(transform.position.x, terrainHeightAtHere()+10, transform.position.z);
                desiredXAngle -= .1f;
            }
            else
                break;
        }
     
     transform.LookAt(target);
     
     
     //check if we can see the player, or does the camera need to move forward
     float margin = 2.5f;
     float step = 1.5f;//4.1f;
     
     
     Vector3 playerPos = tf(alienAndy).position + new Vector3(0,1.0f,0);
     Vector3 here = tf().position;
     float rayDistance = Vector3.Distance(here, playerPos) - margin;
     if(Physics.Raycast(here, playerPos-here, rayDistance))
     {
         if(desiredXAngle > 15)
         {
             desiredXAngle -= step;
         }
     }
     
    }
    float terrainHeightAtHere()
    {
        return terrain.SampleHeight(GetComponent<Transform>().position) + terrain.transform.position.y;
    }
    float radToDeg(float rad) { return rad / (2 * Mathf.PI) * 360; }
    float degToRad(float deg) { return deg / 360 * (2 * Mathf.PI); }
    float unityToUnitCircle(float deg) { return -(degToRad(deg) - Mathf.PI / 2); }
    float unitCircleToUnity(float rad) { return radToDeg(-rad + Mathf.PI / 2); }
    Transform tf() {return GetComponent<Transform>();}
    Transform tf(GameObject other) {return other.GetComponent<Transform>();}
    GameObject[] GetTag(string tag){return GameObject.FindGameObjectsWithTag(tag);}
}
