﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SwapToDungeon1 : MonoBehaviour
{

    void OnTriggerEnter(Collider other)
    {
        FindObjectOfType<GlobalControl>().SaveInventory();
        SceneManager.LoadScene(1);
        GlobalControl.Instance.scene = GlobalControl.Environment.Dungeon1;
        FindObjectOfType<GlobalControl>().LoadInventory();

    }
}
