﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HurtPlayer : MonoBehaviour
{
    public int damageToGive = 1;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other != null)
        {
            if (other.gameObject.tag == "AlienAndy")
            {
                Vector3 hitDirection = other.transform.position - transform.position;
                hitDirection = hitDirection.normalized;
                FindObjectOfType<HealthManager>().HurtPlayer(damageToGive, hitDirection);
            }
            else if (other.gameObject.layer == 9){
                other.GetComponent<NewEnemyScript>().takeDamage(20);
            }
        }
    }
}
