﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SwapToDungeon2 : MonoBehaviour
{

    void OnTriggerEnter(Collider other)
    {
        FindObjectOfType<GlobalControl>().SaveInventory();
        SceneManager.LoadScene(2);
        GlobalControl.Instance.scene = GlobalControl.Environment.Dungeon2;
        FindObjectOfType<GlobalControl>().LoadInventory();

    }
}
