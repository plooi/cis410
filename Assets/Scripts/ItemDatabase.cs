﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemDatabase : MonoBehaviour
{
    public List<Item> items = new List<Item>();

    private void Awake()
    {
        Debug.Log("About to build the database");
        BuildDatabase();
    }
    public Item GetItem(int id)
    {
        return items.Find(item => item.id == id);
    }
    public Item GetItem(string itemName)
    {
        return new Item(items.Find(item => item.title == itemName));
    }

    void BuildDatabase()
    {
        Debug.Log("Building Item Database");
        items = new List<Item>() {
            new Item(0, "Diamond Sword", "Powerful sword",
            new Dictionary<string, int>
            {
                {"Attack", 15}
            }),
            new Item(1, "Diamond Ore", "Can be used to make purchases",
            new Dictionary<string, int>
            {
                {"Value", 444}
       
            }),
            new Item(2, "Iron Sword", "Basic sword",
            new Dictionary<string, int>
            {
                {"Attack", 10}
            }, 1, "Sprites/Items/Iron Sword"),
            new Item(3, "Whip", "Ranged Attack",
            new Dictionary<string, int>
            {
                {"Attack", 6}
            }, 1, "Sprites/Items/WhipIcon")
            
        };
    }
}
