﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class DialogueManager : MonoBehaviour
{

    public Text nameText;
    public Text DialogueText;

    public Animator DialogueBox;
    public Animator Dialogue2;
    
    //keeps track of the sentences in the current dialog
    private Queue<string> sentences;
    public Dialogue start;
    public Dialogue dialogue1;
    public Dialogue dialogue2;
    public Dialogue dialogue3;
    public Dialogue dialogue4;
    public Dialogue dialogue5;
    public Dialogue dialogue6;
    public Dialogue dialogue7;
    public Dialogue dialogue8;
    public Dialogue dialogue9;
    public Dialogue dialogue10;
    public Dialogue dialogue11;
    public Dialogue dialogue12;
    public Dialogue dialogue13;
    public Dialogue dialogue14;
    public Dialogue dialogue15;
    public Dialogue dialogue16;
    public Dialogue dialogue17;
    public Dialogue dialogue18;
    public Dialogue dialogue19;
    public Dialogue dialogue20;

    public static bool startdialman = true;

    void Awake()
    {
        DontDestroyOnLoad(gameObject);

    }

    // Start is called before the first frame update
    void Start ()
    {
        //Initialization of sentences queue
        sentences = new Queue<string>();
       
        RunStartDialogue();
        //StartDialogue(dialogue17);

    }
  
    public void StartDialogue (Dialogue dialogue)
    {
        DialogueBox.SetBool("IsOpen", true);
        nameText.text = dialogue.name;
        sentences.Clear ();

        foreach ( string sentence in dialogue.sentences)
        {
            //takes each sentence from dialogue and adds it to the queue
            sentences.Enqueue(sentence);
        }
        DisplayNextSentence();
    }

    public void DisplayNextSentence()
    {
        // failsafe check to see if we have reached the end of dialog
        if (sentences.Count == 0)
        {
            DialogueBox.SetBool("IsOpen", false);
            return;
        }
        string  sentence = sentences.Dequeue();
        DialogueText.text = sentence;
    }

    public void RunStartDialogue()
    {
        if (startdialman)
        {
            StartDialogue(start);
            startdialman = false;
            
        }
        
    }
    

    public void RunDialogue1()
    {
        StartDialogue(dialogue1);
    }
    public void RunDialogue2()
    {
        //Dialogue2.SetBool("isWalkingNPC", true);
        StartDialogue(dialogue2);
    }
    public void RunDialogue3()
    {
        StartDialogue(dialogue3);
    }
    public void RunDialogue4()
    {
        StartDialogue(dialogue4);
    }
    public void RunDialogue5()
    {
        StartDialogue(dialogue5);
    }
    public void RunDialogue6()
    {
        StartDialogue(dialogue6);
    }
    public void RunDialogue7()
    {
        StartDialogue(dialogue7);
    }
    public void RunDialogue8()
    {
        StartDialogue(dialogue8);
    }
    public void RunDialogue9()
    {
        StartDialogue(dialogue9);
    }
    public void RunDialogue10()
    {
        StartDialogue(dialogue10);
    }
    public void RunDialogue11()
    {
        StartDialogue(dialogue11);
    }
    public void RunDialogue12()
    {
        StartDialogue(dialogue12);
    }
    public void RunDialogue13()
    {
        StartDialogue(dialogue13);
    }
    public void RunDialogue14()
    {
        StartDialogue(dialogue14);
    }
    public void RunDialogue15()
    {
        StartDialogue(dialogue15);
    }
    public void RunDialogue16()
    {
        StartDialogue(dialogue16);
    }
    public void RunDialogue17()
    {
        StartDialogue(dialogue17);
    }
    public void RunDialogue18()
    {
        StartDialogue(dialogue18);
    }
    public void RunDialogue19()
    {
        StartDialogue(dialogue19);
    }
    public void RunDialogue20()
    {
        StartDialogue(dialogue20);
    }
}
