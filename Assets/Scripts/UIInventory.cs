﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIInventory : MonoBehaviour
{
    public List<UIItem> UI_Items = new List<UIItem>();
    public GameObject slotPrefab;
    public Transform slotPanel;
    public int numberOfSlots = 16;

    private void Awake()
    {
        for(int i = 0; i < numberOfSlots; i++)
        {
            GameObject instance = Instantiate(slotPrefab);
            instance.transform.SetParent(slotPanel);
            UI_Items.Add(instance.GetComponentInChildren<UIItem>());
        }
    }

    public void UpdateSlot(int slot, Item item)
    {
        UI_Items[slot].UpdateItem(item);
    }
    public void AddItem(Item item)
    {
        //The next line finds the index of the first empty spot in our list of items. The line after it
        //adds the item to that slot.
        int nextEmpty = UI_Items.FindIndex(i => i.item == null);
        UpdateSlot(nextEmpty, item);
    }
    public void RemoveItem(Item item)
    {
        // This code finds the spot in our list of items that has the passed in item in it, and removes it (by replacing it with null).
        int itemSlot = UI_Items.FindIndex(i => i.item == item);
        UpdateSlot(itemSlot, null);
    }

}


