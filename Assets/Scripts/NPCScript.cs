﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCScript : MonoBehaviour
{
    GameObject alienAndy;
    float talkingDistance = 10;
    bool metMe = false;
    int npcTimer = 0;
    public Waypoint path;
    float speed = .25f;
    float minDistance = 1f;
    public Terrain t;
    float maxDistanceToPlayer = 20;
    public string[] dialogue = new string[]{};
    public string[] itemsToSell;
   
    bool settledDown = false;

    public Animator m_AnimatorNPC;
    public string whichNPCamI;

    AudioSource m_AudioSourceNPC;

    // Start is called before the first frame update
    void Start()
    {
        alienAndy = GameObject.FindGameObjectsWithTag("AlienAndy")[0];
        m_AnimatorNPC = GetComponent<Animator>();
        m_AudioSourceNPC = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if(settledDown)
        {
            if(HDistance(tf().position, tf(alienAndy).position) < 6)
            {
                if(Input.GetKeyDown(KeyCode.T))
                {
                    if (Cursor.lockState == CursorLockMode.Locked)
                    {
                        Cursor.lockState = CursorLockMode.None;
                        ShopUIScript.OpenShopPanel();
                        foreach(string purchasable in itemsToSell)
                        {
                            ShopButtonScript.ShowShopButton(purchasable);
                        }
                        
                    }
                    else
                    {
                        Cursor.lockState = CursorLockMode.Locked;
                        ShopUIScript.CloseShopPanel();
                        ShopButtonScript.HideAllShopButtons();
                    }
                }
            }
        }
    }
    void FixedUpdate()
    {
        resetPhysics();
        if(settledDown)
        {
            
            //do the npc shop stuff
        }
        else
        {
            if (metMe)
            {
                npcTimer += 1;
                
                if(npcTimer < 2)
                {
                    
                }
                else
                {
                    //walk back to village
                    moveTowardWaypoint();
                }
                
                
                
                
            }
            if(!metMe)
            {
                if((Vector3.Distance(alienAndy.GetComponent<Transform>().position, GetComponent<Transform>().position) < talkingDistance))
                {

                    Debug.Log("test3");
                    if(whichNPCamI.Equals("NPC1"))
                        FindObjectOfType<DialogueManager>().RunDialogue2();
                    else if(whichNPCamI.Equals("NPC2"))
                    {
                        FindObjectOfType<DialogueManager>().RunDialogue4();
                        FindObjectsOfType<GlobalControl>()[0].NPC2SpawnInTown = true;
                    }
                    //FindObjectOfType<DialogueManager>().Dialogue2.SetBool("Dialogue2",true);
                    metMe = true;
                }
            }
        }
    }
    void moveTowardWaypoint()
    {
        if(path == null)
        {
           //changes npc animation from walking to idle
            m_AnimatorNPC.SetBool("isWalkingNPC", false);
           // m_AudioSourceNPC.Stop();
            //rotates character to face andy once he reaches town.
            GetComponent<Transform>().Rotate(0, 180, 0);
            if(whichNPCamI.Equals("NPC1"))
                FindObjectOfType<DialogueManager>().RunDialogue3();
            //FindObjectOfType<DialogueManager>().Dialogue3.SetBool("Dialogue3",true);
            settledDown = true;
            return;
        }
        
        
        if(Vector3.Distance(GetComponent<Transform>().position, alienAndy.GetComponent<Transform>().position) > maxDistanceToPlayer)
        {
            //changes npc animation from walking to idle
            m_AnimatorNPC.SetBool("isWalkingNPC", false);
            m_AudioSourceNPC.Stop();
            return;
        }
        //changes npc animation from idle to walking
        m_AnimatorNPC.SetBool("isWalkingNPC", true);
        m_AudioSourceNPC.Play();

        Vector3 oldPos = GetComponent<Transform>().position;
        
        
        float horizontalUnitCircleAngleToWaypoint = HAngle(GetComponent<Transform>().position, path.GetComponent<Transform>().position);
        Vector3 lerp = GetComponent<Transform>().position + HMove(horizontalUnitCircleAngleToWaypoint, speed);
        
        
        
        GetComponent<Transform>().position = new Vector3(lerp.x, GetComponent<Transform>().position.y, lerp.z);
        /*GetComponent<Transform>().position = new Vector3(
            GetComponent<Transform>().position.x,
            //t.SampleHeight(GetComponent<Transform>().position) + t.transform.position.y,
            GetComponent<Transform>().position.z);*/
        float angle = RadToDeg(UnitCircleToUnityAngle(GetAngle(
            GetComponent<Transform>().position.z, 
            GetComponent<Transform>().position.x,
            path.GetComponent<Transform>().position.z, 
            path.GetComponent<Transform>().position.x
            )));
        GetComponent<Transform>().eulerAngles = new Vector3(0, angle ,0);
        GetComponent<Rigidbody>().velocity = new Vector3(0,GetComponent<Rigidbody>().velocity.y,0);
        GetComponent<Rigidbody>().angularVelocity = new Vector3(0,0,0);
        
        if(distance2d(GetComponent<Transform>().position, path.GetComponent<Transform>().position) < minDistance)
        {

           
            path = path.next;        
        
        }
    }
    float distance2d(Vector3 a, Vector3 b)
    {
        return Mathf.Sqrt(Mathf.Pow(a.x-b.x,2) + Mathf.Pow(a.z-b.z,2));
    }
    void resetPhysics()
    {
        GetComponent<Transform>().eulerAngles = new Vector3(0, GetComponent<Transform>().eulerAngles.y,0);
        GetComponent<Rigidbody>().velocity = new Vector3(0,GetComponent<Rigidbody>().velocity.y,0);
    }
    float UnitCircleToUnityAngle(float unitCircle)
    {
        return -unitCircle + Mathf.PI/2;
    }
    float RadToDeg(float rad)
    {
        return rad/(Mathf.PI*2)*360;
    }
    float GetAngle(float z1, float x1, float z2, float x2)
    {
        if(x1==x2)
        {
            if(z1>=z2)
            {
                return Mathf.PI/2;
            }
            else
            {
                return 3*Mathf.PI/2;
            }
        }
        if(x1 > x2)
            return Mathf.Atan((z2-z1)/(x2-x1)) + Mathf.PI;
        else
            return Mathf.Atan((z2-z1)/(x2-x1));
    }
    
    float HDistance(Vector3 a, Vector3 b){ return Sqrt(Sq(a.x-b.x) + Sq(a.z-b.z)); }
    float Sq(float f) { return f*f; }
    float Sqrt(float f) { return Mathf.Sqrt(f); }
    float HAngle(Vector3 a, Vector3 b) { return _GetAngle(a.z,a.x,b.z,b.x); } float _GetAngle(float z1, float x1, float z2, float x2) { if(x1==x2) { if(z1>=z2) { return Mathf.PI/2; } else { return 3*Mathf.PI/2; } } if(x1 > x2) return Mathf.Atan((z2-z1)/(x2-x1)) + Mathf.PI; else return Mathf.Atan((z2-z1)/(x2-x1)); }
    Vector3 HMove(float angle, float distance) { return new Vector3(distance*Mathf.Cos(angle),0,distance*Mathf.Sin(angle)); }
    T g<T>() { return GetComponent<T>(); }
    Transform tf() {return GetComponent<Transform>();}
    Transform tf(GameObject other) {return other.GetComponent<Transform>();}
    GameObject[] GetTag(string tag){return GameObject.FindGameObjectsWithTag(tag);}
}
