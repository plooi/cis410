﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tooltip : MonoBehaviour
{
    public Text tooltipText;
    // Start is called before the first frame update
    void Start()
    {
        tooltipText.gameObject.SetActive(false);
    }

    public void GenerateTooltip(Item item)
    {
        string statText = "";
        if(item.stats.Count > 0)
        {
            foreach (var stat in item.stats)
            {
                statText += stat.Key.ToString() + ": " + stat.Value.ToString() + "\n";
            }
        }
        
        string tooltip;
        
        
        
        
        
        if(item.quantity == 1)
        {
            tooltip = string.Format("{0}\n{1}\n\n{2}", item.title, item.description, statText);
        }
        else
        {
            if(item.title.EndsWith("s") || item.title.EndsWith("x"))
            {
                tooltip = string.Format("{3} {0}es \n{1}\n\n{2}", item.title, item.description, statText, item.quantity);
            }
            else
            {
                tooltip = string.Format("{3} {0}s \n{1}\n\n{2}", item.title, item.description, statText, item.quantity);
            }
        }
        
        tooltipText.text = tooltip;
        gameObject.SetActive(true);
    }




}
