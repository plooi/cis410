/*
Author: Peter Looi

*/



﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewEnemyScript : MonoBehaviour
{

    void NewEvent(){}
    
    
    
    
    private float respawnDistance = 240;
    private float alertDistance = 38;
    public GameObject drop;
    public GameObject blood;
    // Start is called before the first frame update
    Transform t;
    private GameObject alienAndy;
    
    
    float maxHealth = 30f;
    float health;
    public int damage = 1;
    private float speed= .09f;//forward move speed
    private float chargeSpeed= .13f;
    private float forwardSpeed;
    private float sidewaysSpeed;//sideways speed vector
    private float forwardChargeSpeed;
    private float sidewaysChargeSpeed;//sideways speed vector
    private float circleSpeed;//How fast does this enemy move when it circles around the player?
    private float clockOrCounter;//which direction does this enemy circle around the player? Clockwise or counter clockwise? 1 or -1
    float circlingDistance;//the distance at which the enemy just starts circling around the player
    float attackingDistance = 2.1f;//how far away from the player does the enemy have to be in order to do damage
    float noticePlayerDistance = 20f; //how far away from the player does the enemy have to be in order to notice the player
    float forgetAboutPlayerDistance = 55f;
    float advanceChance = .001f;
    float ps; //prefab scale
    Animator anim;
    int animationWalkSpeed = 25;
    
    
    
    
    
    
    
    public string enemyCategory = "None";
    
    //float friction = .9f;
    float knockback = 6f;
    float knockup = 6f;
    bool alive = true;
    
    int state = 0;
    //0 is idle
    //1 is moving up
    //2 is circling
    //3 is closingIn
    //4 is attacking
    //5 is knockback
    //6 is dying
    int lastState = 0;
    
    
    
    
    
    
    
    
    
    int dieTimer = 0;
    int dieTime;
    int cc;
    
    
    float lastz = 0;
    float lastx = 0;
    long timer = 0;
    
    int attackTimer = -1;
    
    
    
    
    
    
    Vector3 originalPosition;
    
    
    
    public void takeDamage(float health)
    {
    
        if(alive)
        {
            for(int i = 0; i < 10; i++)
            {
                GameObject b = Instantiate(blood, tf().position, Quaternion.identity);
                b.GetComponent<BloodPScript>().Init(gameObject,2f);
            }
            this.health -= health;
            float angle = GetAngle2();
            GetComponent<Rigidbody>().velocity = new Vector3(-knockback*Mathf.Cos(angle),knockup,-knockback*Mathf.Sin(angle));
            t.position += new Vector3(0f,.1f,0f);
            state = 5;
        }
            
    }
    int randomM1P1()
    {
    
        if(Random.Range(0f,1f) < .5)
        {
            return 1;
        }
        else
        {
            return -1;
        }
    }
    void Start()
    {
        health = maxHealth;
        originalPosition = new Vector3(tf().position.x, tf().position.y, tf().position.z);
        AnimationClip clip;
        
        AnimationEvent evt;
        evt = new AnimationEvent();
        evt.intParameter = 12345;
        evt.time = 1.3f;
        evt.functionName = "NewEvent";
        anim = GetComponent<Animator>();
        clip = anim.runtimeAnimatorController.animationClips[0];
        clip.AddEvent(evt);
        
        
        ps = GetComponent<Transform>().localScale.x;
        
        
        
        
        dieTime = Random.Range(200,400);
        t = GetComponent<Transform>();
        alienAndy = GameObject.FindGameObjectsWithTag("AlienAndy")[0];
        clockOrCounter = randomM1P1();
        
        
        circlingDistance = Random.Range(8f,13f);
        circleSpeed= Random.Range(.03f,.08f);
        
        cc = randomM1P1();
        SetSpeeds();
        
        
        
        
        
        //anim.SetFloat("walkSpeed", 0f);
        //anim.SetBool("walk",true);
    }
    
    //alert nearby enemies
    void Alert()
    {
        foreach(GameObject enemy in GetTag("Enemy"))
        {
            if(HDistance(tf().position, tf(enemy).position) < alertDistance)
            {
                if(enemy.GetComponent<NewEnemyScript>() != null)
                {
                    if(enemy.GetComponent<NewEnemyScript>().state == 0)
                    {
                        enemy.GetComponent<NewEnemyScript>().state = 1;
                    }
                }
            }
        }
    }
    void Walk()
    {
        anim.SetInteger("walkSpeed",animationWalkSpeed);
        anim.SetBool("attackReady", false);
        anim.SetInteger("attackID", 0);
    }
    void StopWalk()
    {
        anim.SetInteger("walkSpeed",0);
        anim.SetBool("attackReady", false);
        anim.SetInteger("attackID", 0);
    }
    void Attack()
    {
        anim.SetInteger("walkSpeed",0);
        anim.SetBool("attackReady", true);
        anim.SetInteger("attackID", 1);
    }
    void SetSpeeds()
    {
        float f = Random.Range(.4f,.8f);
        forwardSpeed = f*speed;
        sidewaysSpeed = (1-f)*speed;
        forwardChargeSpeed = f*chargeSpeed;
        sidewaysChargeSpeed = (1-f)*chargeSpeed;
    }
    void SetLast()
    {
        lastz = g<Transform>().position.z;
        lastx = g<Transform>().position.x;
    }
    bool onGround()
    {
        //Debug.DrawRay(t.position + .005f * GetComponent<CapsuleCollider>().center,Vector3.down, Color.yellow, 1f);
        float scale = ps;
        float r = GetComponent<CapsuleCollider>().radius * scale;
        r = .2f;
        
        //return Physics.Raycast(GetComponent<CapsuleCollider>().center+t.position,Vector3.down, GetComponent<CapsuleCollider>().height/2+.01f);
        //print((GetComponent<CapsuleCollider>().height/2+.01f)*scale);
        return Physics.BoxCast(
        GetComponent<CapsuleCollider>().center * scale + t.position,
        new Vector3(r,.01f,r),
        Vector3.down, 
        Quaternion.identity, 
        (GetComponent<CapsuleCollider>().height/2)*scale+.01f);
        
        
    }
    // Update is called once per frame
    void idle()
    {
        noHorizontal();
        //a.CrossFade("Idle");
        StopWalk();
        
        if(GetDistance() < noticePlayerDistance)state = 1;
        standUp();
    }
    
    void movingUp()
    {
        noHorizontal();
        float angle = GetAngle2();
        //a.CrossFade("Walk");
        Walk();
        //t.position += new Vector3(speed*Mathf.Cos(angle),0,speed*Mathf.Sin(angle));
        MoveTowardsPlayer(forwardSpeed,sidewaysSpeed);
        if(GetDistance() < circlingDistance)state = 3;//2;
        idleIfFarAway();
        //GetComponent<Rigidbody>().velocity = new Vector3(0,0,0);
        
        standUpAndFacePlayer();
    }
    void circling()
    {
        noHorizontal();
        //a.CrossFade("Walk")
        Walk();
        float angle = GetAngle2();
        t.position += new Vector3(clockOrCounter*circleSpeed*Mathf.Cos(angle+Mathf.PI/2),0,clockOrCounter*circleSpeed*Mathf.Sin(angle+Mathf.PI/2));
        t.position += new Vector3(circleSpeed*Mathf.Cos(angle),0,circleSpeed*Mathf.Sin(angle));
        if(Random.Range(0f,1f) < advanceChance)state = 3;
        if(GetDistance() < attackingDistance)state = 4;
        GetComponent<Rigidbody>().velocity = new Vector3(0,0,0);
        standUpAndFacePlayer();
        idleIfFarAway();
    }
    void closingIn()
    {
        noHorizontal();
        float angle = GetAngle2();
        //a.CrossFade("Walk");
        Walk();
        //t.position += new Vector3(speed*Mathf.Cos(angle),0,speed*Mathf.Sin(angle));
        MoveTowardsPlayer(forwardChargeSpeed, sidewaysChargeSpeed);
        if(GetDistance() < attackingDistance)state = 4;
        //GetComponent<Rigidbody>().velocity = new Vector3(0,0,0);
        standUpAndFacePlayer();
        //idleIfFarAway();
    
    }
    void attacking()
    {
        noHorizontal();
        //a.CrossFade("Lumbering");
        Attack();
        alienAndy.GetComponent<PlayerMovement>().Damage(.1f);
        
        
        if(attackTimer == 0)
        {
            //Ben Put this here. Damages player, handles knockback
            Vector3 hitDirection = alienAndy.transform.position - transform.position;
            hitDirection = hitDirection.normalized;
            FindObjectOfType<HealthManager>().HurtPlayer(damage, hitDirection);
            //End of Ben's code
            
        }
        if(attackTimer <= -1)
        {
            attackTimer = 60;
        }

        if (GetDistance() > attackingDistance) state = 3;
        //GetComponent<Rigidbody>().velocity = new Vector3(0,0,0);
        GetComponent<Rigidbody>().angularVelocity = new Vector3(0,0,0);
        standUpAndFacePlayer();
        idleIfFarAway();
        
    }
    void doKnockback()
    {
        //a.CrossFade("Idle");
        Walk();
        standUpAndFacePlayer();
        GetComponent<Rigidbody>().angularVelocity = new Vector3(0,0,0);
        if(onGround())
        {
            state = 3;
            GetComponent<Rigidbody>().velocity = new Vector3(0,0,0);
        }
    }
    void dying()
    {
        if(alive)
        {
            alive=false;
            t.Rotate(new Vector3(Random.Range(-20f,20f),Random.Range(-20f,20f),Random.Range(-20f,20f)));
            //g<Rigidbody>().angularVelocity = new Vector3(Random.Range(-10f,10f),Random.Range(-10f,10f),Random.Range(-10f,10f));
            StopWalk();
            Drops();
            //a.Stop();
            //GetComponent<Rigidbody>().velocity = new Vector3(GetComponent<Rigidbody>().velocity.x*friction, GetComponent<Rigidbody>().velocity.y*friction, GetComponent<Rigidbody>().velocity.z*friction);
        }
        
        
        /*if(dieTimer >= dieTime)
        {
            
            Destroy(gameObject);
        }*/
        
        
    }
    void noHorizontal()
    {
        GetComponent<Rigidbody>().velocity = new Vector3(0,GetComponent<Rigidbody>().velocity.y,0);
    }
    void Respawn()
    {
        tf().position = originalPosition;
        state = 0;
        health = maxHealth;
        alive = true;
    }
    void FixedUpdate()
    {
    
    
        if(lastState == 0 && state != 0)
        {
            Alert();
        }
        lastState = state;
        
        
        
        if(HDistance(tf().position, tf(alienAndy).position) > respawnDistance)
        {
            Respawn();
        }
        
        
        //print(anim.GetBool("attackReady"));
        timer ++;
        attackTimer--;
        if(this.health <= 0)
        {
            
            die();
            //Destroy(gameObject);
            //always put destroy at the end of the function, otherwise it might(will) try to do the rest of the function after it's been destroyed
        }
        //print(""+GetComponent<Rigidbody>().velocity);
        //print(onGround());
        if(state == 0)idle();
        else if(state == 1)movingUp();
        else if(state == 2)circling();
        else if(state == 3)closingIn();
        else if(state == 4)attacking();
        else if(state == 5)doKnockback();
        else if(state == 6)dying();
        
        
        
    }
    void MoveTowardsPlayer(float forwardSpeed, float sidewaysSpeed)
    {
        
        float angle = GetAngle2();
        
        t.position += HMove(angle+Mathf.PI/2,cc*sidewaysSpeed);
        t.position += HMove(angle, forwardSpeed);
        
        //if(timer % 60 == 0)
        if(timer % 30 == 0)
        {
            //if(HDistance(new Vector3(lastx, 0, lastz), g<Transform>().position) < 1.5 || Random.Range(0f,1f) < .1)
            if(HDistance(new Vector3(lastx, 0, lastz), g<Transform>().position) < .8 || Random.Range(0f,1f) < .1)
            {
                cc *= -1;
                SetSpeeds();
            }
            
            SetLast();
        }
        
    }
    void die()
    {
        state = 6;
    }
    void standUp()
    {
        t.eulerAngles = new Vector3(0,t.eulerAngles.y,0);
    }
    void standUpAndFacePlayer()
    {
        float angle = GetAngle2();
        t.eulerAngles = new Vector3(0,RadToDeg(UnitCircleToUnityAngle(angle)),0);
    }
    void idleIfFarAway()
    {
        if(GetDistance() > forgetAboutPlayerDistance)
        {
            state = 0;
        }
    }
        
    void Drops()
    {
        GameObject drops = Instantiate(drop, GetComponent<Transform>().position + Vector3.up*1, Quaternion.identity);
        drops.GetComponent<DropScript>().Init(drops.GetComponent<DropScript>().diamondMaterial);
        if(isLast())
        {
            if(enemyCategory.Equals("TutorialEnemy"))
            {
                GameObject[] gateBlocks = GameObject.FindGameObjectsWithTag("TutorialGate");
                foreach(GameObject gate in gateBlocks)
                {
                    Destroy(gate);
                   
                }
                //Dialogue1.SetBool("Dialogue1",true);
                FindObjectOfType<DialogueManager>().RunDialogue1();
                FindObjectOfType<DialogueManager>().RunStartDialogue();
                //  FindObjectOfType<DialogueManager>().Dialogue1.SetBool("Dialogue1",true);
                //TempMessageBox.Show("Congratulations! You have defeated the first dungeon. A door will have opened now. Exit the dungeon, and go find the first villager. There may be enemies along the way.");
            }
            
            /*
            else if(enemyCategory.Equals("ANother category..."))
            {
                do something else
            }
            */
        }
        
    }
    /*
    Returns if this enemy is the last enemy
    with this category. If no other enemies have
    this same category as this object
    */
    bool isLast()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
        List<GameObject> enemiesInMyCategory = new List<GameObject>();
        foreach(GameObject e in enemies)
        {
            /*if(e.GetComponent<EnemyScript>() != null)
            {
                if(e.GetComponent<EnemyScript>().enemyCategory.Equals(enemyCategory) && e.GetComponent<EnemyScript>().state != 6)
                {
                    enemiesInMyCategory.Add(e);
                }
            }*/
            if(e.GetComponent<NewEnemyScript>() != null)
            {
                if(e.GetComponent<NewEnemyScript>().enemyCategory.Equals(enemyCategory) && e.GetComponent<NewEnemyScript>().state != 6)
                {
                    enemiesInMyCategory.Add(e);
                }
            }
        }
        
        return enemiesInMyCategory.Count == 0;
        
        
        
    }
    float Sq(float x)
    {
        return x*x;
    }
    float Sqrt(float x)
    {
        return Mathf.Sqrt(x);
    }
    float GetDistance()
    {
        Transform other = alienAndy.GetComponent<Transform>();
        return Sqrt(Sq(t.position.x-other.position.x) + Sq(t.position.z-other.position.z));
    }
    float GetAngle2()
    {
        Transform other = alienAndy.GetComponent<Transform>();
        return GetAngle(t.position.z,t.position.x,other.position.z,other.position.x);
    }
    
    //rad->rad
    float UnitCircleToUnityAngle(float unitCircle)
    {
        return -unitCircle + Mathf.PI/2;
    }
    float RadToDeg(float rad)
    {
        return rad/(Mathf.PI*2)*360;
    }
    float GetAngle(float z1, float x1, float z2, float x2)
    {
        if(x1==x2)
        {
            if(z1>=z2)
            {
                return Mathf.PI/2;
            }
            else
            {
                return 3*Mathf.PI/2;
            }
        }
        if(x1 > x2)
            return Mathf.Atan((z2-z1)/(x2-x1)) + Mathf.PI;
        else
            return Mathf.Atan((z2-z1)/(x2-x1));
    }
    
    
    
    
    
    
    
    GameObject[] GetTag(string tag){return GameObject.FindGameObjectsWithTag(tag);}
    
    
    
    
    
    
    
    
    float HDistance(Vector3 a, Vector3 b)
    {
        return Sqrt(Sq(a.x-b.x) + Sq(a.z-b.z));
    }
    
    float HAngle(Vector3 a, Vector3 b)
    {
        return _GetAngle(a.z,a.x,b.z,b.x);
    }
    float _GetAngle(float z1, float x1, float z2, float x2)
    {
        if(x1==x2)
        {
            if(z1>=z2)
            {
                return Mathf.PI/2;
            }
            else
            {
                return 3*Mathf.PI/2;
            }
        }
        if(x1 > x2)
            return Mathf.Atan((z2-z1)/(x2-x1)) + Mathf.PI;
        else
            return Mathf.Atan((z2-z1)/(x2-x1));
    }
   
    Vector3 HMove(float angle, float distance)
    {
        return new Vector3(distance*Mathf.Cos(angle),0,distance*Mathf.Sin(angle));
    }
    T g<T>()
    {
        return GetComponent<T>();
    }
    T gg<T>(GameObject go)
    {
        return go.GetComponent<T>();
    }
    Transform tf() {return GetComponent<Transform>();}
    Transform tf(GameObject other) {return other.GetComponent<Transform>();}



}
