﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item
{
    public int id;
    public string title;
    public string description;
    public Sprite icon;
    public int quantity = 1;
    public Dictionary<string, int> stats = new Dictionary<string, int>();

    public Item(int id, string title, string description, Dictionary<string,int> stats)
    {
        Debug.Log("Adding item");
        this.id = id;
        this.title = title;
        this.description = description;
        this.icon = Resources.Load<Sprite>("Sprites/Items/" + title);
        this.stats = stats;
    }
    public Item(int id, string title, string description, Dictionary<string,int> stats, int quantity)
    {
        Debug.Log("Adding item");
        this.id = id;
        this.title = title;
        this.description = description;
        this.icon = Resources.Load<Sprite>("Sprites/Items/" + title);
        this.stats = stats;
        this.quantity = quantity;
    }
    public Item(int id, string title, string description, Dictionary<string,int> stats, int quantity, string iconPath)
    {
        Debug.Log("Adding item");
        this.id = id;
        this.title = title;
        this.description = description;
        this.icon = Resources.Load<Sprite>(iconPath);
        this.stats = stats;
        this.quantity = quantity;
    }
    
    public Item(Item item)
    {
        this.id = item.id;
        this.title = item.title;
        this.description = item.description;
        this.icon = item.icon;//Resources.Load<Sprite>("Sprites/Items/" + item.title);
        this.stats = item.stats;
        this.quantity = item.quantity;
    }
}
