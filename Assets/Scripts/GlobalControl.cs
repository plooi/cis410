﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalControl : MonoBehaviour
{
    public int health;
    public Vector3 position;
    public static GlobalControl Instance;
    
    
    public bool NPC2SpawnInTown = false;//this is the only way i could think of how to get the npc to spawn in town
    public GameObject NPC2Prefab;
    
    List<Item> itemSaveBetweenScenes = new List<Item>();
    
    
    
    public enum Environment : int
    {
        Start,
        Overworld,
        Dungeon1,
        Dungeon2
    }

    public Environment scene;

    void Awake()
    {
        if(Instance == null)
        {
            DontDestroyOnLoad(gameObject);
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }
    void Update()
    {
        
        if(scene == Environment.Overworld)
        {
            //print("Overworld" + NPC2SpawnInTown);
            if(NPC2SpawnInTown)
            {
                if(GameObject.FindGameObjectsWithTag("Doc").Length > 0)
                {
                    GameObject.FindGameObjectsWithTag("Doc")[0].GetComponent<Transform>().Find("Alien").gameObject.GetComponent<SkinnedMeshRenderer>().enabled = true;
                    NPC2SpawnInTown = false;
                }
                
            }
            
            
        }
        else
        {
            //print("not Overworld" + NPC2SpawnInTown);
        }
    }
    public void SaveInventory()
    {
        itemSaveBetweenScenes = DeepCopy(GameObject.FindGameObjectsWithTag("AlienAndy")[0].GetComponent<Inventory>().itemList);
    }
    public void LoadInventory()
    {
        loadInventoryTimer = 3;
    }
    int loadInventoryTimer = -1;
    List<Item> DeepCopy(List<Item> a)
    {
        List<Item> ret = new List<Item>();
        foreach(Item item in a)
        {
            ret.Add(new Item(item));
            print("copying item with quantity " + item.quantity);
        }
        
        return ret;
    }
    void FixedUpdate()
    {
        loadInventoryTimer -= 1;
        if(loadInventoryTimer == 0)
        {
            foreach(Item item in new List<Item>(GameObject.FindGameObjectsWithTag("AlienAndy")[0].GetComponent<Inventory>().itemList))
                GameObject.FindGameObjectsWithTag("AlienAndy")[0].GetComponent<Inventory>().RemoveItem(item);
            foreach(Item item in itemSaveBetweenScenes)
            {
                for(int i = 0; i < item.quantity; i++)
                {
                    GameObject.FindGameObjectsWithTag("AlienAndy")[0].GetComponent<Inventory>().AddItem(item.title);
                }
            }
        }
        
    }
    void printQuantities(string tag)
    {
        print(tag);
        foreach(Item item in GameObject.FindGameObjectsWithTag("AlienAndy")[0].GetComponent<Inventory>().itemList)
        {
            print(tag+"-q" + item.quantity);
        }
    }
    
}
