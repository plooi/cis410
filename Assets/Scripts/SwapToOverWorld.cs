﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SwapToOverWorld : MonoBehaviour
{

    public GameObject player;
    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject == player)
        {
            FindObjectOfType<GlobalControl>().SaveInventory();
            GlobalControl.Instance.scene = GlobalControl.Environment.Overworld;
            SceneManager.LoadScene(0);
            FindObjectOfType<GlobalControl>().LoadInventory();
        }
        

    }
}
