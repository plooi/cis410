﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueTrigger : MonoBehaviour
{

    //Dialogue references the script Dialogue.cs
    public Dialogue dialogue;

    // this is the method used to trigger our dialogue calls 

    public void TriggerDialogue () 
    {
        FindObjectOfType<DialogueManager>().StartDialogue(dialogue);
        
    }


}
