﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropScript : MonoBehaviour
{
    // Start is called before the first frame update
    public Material diamondMaterial;
    GameObject child;
    GameObject alienAndy;
    void Start()
    {
        
    }
    public void Init(Material m)
    {
        child = GetComponent<Transform>().GetChild(0).gameObject;
        GetComponent<Renderer>().material = m;
        child.GetComponent<Renderer>().material = m;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(alienAndy == null)
        {
            if(GameObject.FindGameObjectsWithTag("AlienAndy").Length > 0)
            {
                alienAndy = GameObject.FindGameObjectsWithTag("AlienAndy")[0];
            }
            else
            {
                return;
            }
        }
        GetComponent<Transform>().eulerAngles += new Vector3(0,1,0);
        if(HDistance(tf().position, tf(alienAndy).position) < 1.5)
        {
            alienAndy.GetComponent<Inventory>().AddItem("Diamond Ore");
            Destroy(gameObject);
        }
    }
    
    
    
    
    
    float HDistance(Vector3 a, Vector3 b){ return Sqrt(Sq(a.x-b.x) + Sq(a.z-b.z)); }
    float Sq(float f) { return f*f; }
    float Sqrt(float f) { return Mathf.Sqrt(f); }
    float HAngle(Vector3 a, Vector3 b) { return _GetAngle(a.z,a.x,b.z,b.x); } float _GetAngle(float z1, float x1, float z2, float x2) { if(x1==x2) { if(z1>=z2) { return Mathf.PI/2; } else { return 3*Mathf.PI/2; } } if(x1 > x2) return Mathf.Atan((z2-z1)/(x2-x1)) + Mathf.PI; else return Mathf.Atan((z2-z1)/(x2-x1)); }
    float UnitCircleToUnityAngle(float unitCircle) { return -unitCircle + Mathf.PI/2; }
    float RadToDeg(float rad) { return rad/(Mathf.PI*2)*360; }
    Vector3 HMove(float angle, float distance) { return new Vector3(distance*Mathf.Cos(angle),0,distance*Mathf.Sin(angle)); }
    T g<T>() { return GetComponent<T>(); }
    Transform tf() {return GetComponent<Transform>();}
    Transform tf(GameObject other) {return other.GetComponent<Transform>();}
}
