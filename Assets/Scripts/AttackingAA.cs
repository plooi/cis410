﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackingAA : MonoBehaviour
{

    AudioSource m_AudioSourceAttack;
    int cooldown = 0;
    int timer = 0;
    void Awake(){DontDestroyOnLoad(gameObject);}
    // Start is called before the first frame update
    void Start()
    {
        m_AudioSourceAttack = GetComponent<AudioSource>();
    }

    void FixedUpdate()
    {
        cooldown -= 1;
        timer -= 1;
        if(timer == 0)
        {
            m_AudioSourceAttack.Play();
        }
        
    }
    // Update is called once per frame
    void Update()
    {
        
        if (Input.GetMouseButtonDown(0) && cooldown <=0)
        {
            timer = 10;
            cooldown = 55;
        }
        
    }
}
